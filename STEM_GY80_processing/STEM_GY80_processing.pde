/*
STEM_GY80 by Christopher Hwang
www.educaltion.com
January 2015 V1.0
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version. see <http://www.gnu.org/licenses/>
*/

import processing.serial.Serial; // serial library
import controlP5.*; // controlP5 library
import processing.opengl.*;
import java.lang.StringBuffer; // for efficient String concatemation
import javax.swing.SwingUtilities; // required for swing and EDT
import javax.swing.JFileChooser; // Saving dialogue
import javax.swing.filechooser.FileFilter; // for our configuration file filter
import javax.swing.JOptionPane; // for message dialogue

//Added For  Processing 2.0.x compabillity
import java.util.*;
import java.io.*;
//****************************


PrintWriter output;
BufferedReader reader;
String portnameFile="SerialPort.txt"; // Name of file for Autoconnect.
int GUI_BaudRate = 57600; // Default.
int SerialPort;
Serial g_serial;
ControlP5 controlP5;
//Textlabel txtlblWhichcom;

DropdownList commListbox;

int commListMax;
int tabHeight=30; // Extra height needed for Tab

cGraph g_graph;
int windowsX    = 1000;       int windowsY    = 540+tabHeight;
int xGraph      = 10;         int yGraph      = 325+tabHeight;
int xObj        = 800;        int yObj        = 405+tabHeight;
int xCompass    = 800;        int yCompass    = 120+tabHeight;
int xLevelObj   = 500;        int yLevelObj   = 120+tabHeight;
int xTempObj    = 200;        int yTempObj    = 120+tabHeight;
int xParam      = 520;        int yParam      = 5;
int xButton     = 850;        int yButton     = 5;

int i, j; // enummerators
int cycleTime, i2cError, version, horizonInstrSize, init_com, graph_on;

//int byteMP[] = new int[8];  // Motor Pins.  Varies by multiType and Arduino model (pro Mini, Mega, etc).

float gx, gy, gz, ax, ay, az, magx, magy, magz, alt, temp, head, angx, angy, angyLevelControl, angCalc;

boolean axGraph   = true,  ayGraph   = true,  azGraph   = true,
        gxGraph   = true,  gyGraph   = true,  gzGraph   = true,
        magxGraph = true,  magyGraph = true,  magzGraph = true,
        altGraph  = true,  headGraph = true,  tempGraph = true;

boolean	hideDraw  = false, GraphicsInited = false, graphEnabled = false, Mag_ = false;

cDataArray accPITCH   = new cDataArray(200), accROLL    = new cDataArray(200), accYAW     = new cDataArray(200),
           gyroPITCH  = new cDataArray(200), gyroROLL   = new cDataArray(200), gyroYAW    = new cDataArray(200),
	   magxData   = new cDataArray(200), magyData   = new cDataArray(200), magzData   = new cDataArray(200),
	   altData    = new cDataArray(200), headData   = new cDataArray(200), tempData   = new cDataArray(200);

Slider  axSlider, aySlider, azSlider,
        gxSlider, gySlider, gzSlider,
        magxSlider, magySlider, magzSlider,
        altSlider, headSlider, tempSlider,scaleSlider;

Button  buttonCALIBRATE_ACC, buttonCALIBRATE_MAG,
	buttonAcc, buttonBaro, buttonMag,
        buttonSTART, buttonSTOP, btnQConnect;

Toggle tACC_ROLL, tACC_PITCH, tACC_Z, tGYRO_ROLL, tGYRO_PITCH, tGYRO_YAW, tBARO, tHEAD, tMAGX, tMAGY, tMAGZ, tTEMP;

// Colors
color yellow_ = color(200, 200, 20), green_ = color(30, 120, 30), red_ = color(120, 30, 30), blue_ = color(50, 50, 100),
	   grey_ = color(30, 30, 30),black_ = color(0, 0, 0),orange_ =color(200,128,0);

PFont font8, font9, font12, font15, font40;

// coded by Eberhard Rensch
// Truncates a long port name for better (readable) display in the GUI
String shortifyPortName(String portName, int maxlen)  {
  String shortName = portName;
  if(shortName.startsWith("/dev/")) shortName = shortName.substring(5);
  if(shortName.startsWith("tty.")) shortName = shortName.substring(4); // get rid of leading tty. part of device name
  if(portName.length()>maxlen) shortName = shortName.substring(0,(maxlen-1)/2) + "~" +shortName.substring(shortName.length()-(maxlen-(maxlen-1)/2));
  if(shortName.startsWith("cu.")) shortName = "";// only collect the corresponding tty. devices
  return shortName;
}

controlP5.Controller hideLabel(controlP5.Controller c) {
  c.setLabel("");
  c.setLabelVisible(false);
  return c;
}

void setup() {
  size(windowsX,windowsY,OPENGL);
  frameRate(20);

  font8 = createFont("Arial bold",8,false);
  font9 = createFont("Arial bold",9,false);
  font12 = createFont("Arial bold",12,false);
  font15 = createFont("Arial bold",15,false);
  font40 = createFont("Arial bold",40,false);

  controlP5 = new ControlP5(this); // initialize the GUI controls
  controlP5.setControlFont(font12);

  // IMU data graph
  g_graph  = new cGraph(xGraph+115,yGraph, 480, 200);

  // Comm port list
  commListbox = controlP5.addDropdownList("portComList",5,26,150,240).setItemHeight(20).setBarHeight(20);
  commListbox.captionLabel().set("COMM PORT");
  for( i=0;i<Serial.list().length;i++) {
	String pn = shortifyPortName(Serial.list()[i], 13);
	if (pn.length() >0 ) commListbox.addItem(pn,i); // addItem(name,value)
	commListMax = i;
  }
  commListbox.addItem("CLOSE COMM",++commListMax); // addItem(name,value)
  // text label for which comm port selected
  //txtlblWhichcom = controlP5.addTextlabel("txtlblWhichcom","No Port Selected",5,65+tabHeight); // textlabel(name,text,x,y)

  btnQConnect = controlP5.addButton("bQCONN",1,165,5,96,20).setLabel("  ReConnect").setColorBackground(red_).hide();
  buttonSTART  = controlP5.addButton("bSTART",1,280,5,61,20).setLabel("  START").setColorBackground(red_);
  buttonSTOP   = controlP5.addButton("bSTOP",1,347,5,55,20).setLabel("  STOP").setColorBackground(red_);

  buttonAcc   = controlP5.addButton("bACC",1,xButton,yButton,45,20).setColorBackground(red_).setLabel("ACC");
  buttonBaro  = controlP5.addButton("bBARO",1,xButton+50,yButton,45,20).setColorBackground(red_).setLabel("BARO");
  buttonMag   = controlP5.addButton("bMAG",1,xButton+100,yButton,45,20).setColorBackground(red_).setLabel("MAG");

  color c,black;
  black = color(0,0,0);
  int xo = xGraph-7;
  int x = xGraph+40;
  int y1= yGraph+10;  //ACC
  int y2= yGraph+55;  //GYRO
  int y5= yGraph+100; //MAG
  int y3= yGraph+150; //ALT
  int y4= yGraph+165; //HEAD
  int y6= yGraph+180; //TEMP

  tACC_ROLL =       controlP5.addToggle("ACC_ROLL",true,x,y1+10,20,10).setColorActive(color(255, 0, 0)).setColorBackground(black).setLabel("");
  tACC_PITCH =      controlP5.addToggle("ACC_PITCH",true,x,y1+20,20,10).setColorActive(color(0, 255, 0)).setColorBackground(black).setLabel("");
  tACC_Z =          controlP5.addToggle("ACC_Z",true,x,y1+30,20,10).setColorActive(color(0, 0, 255)).setColorBackground(black).setLabel("");
  tGYRO_ROLL =      controlP5.addToggle("GYRO_ROLL",true,x,y2+10,20,10).setColorActive(color(200, 200, 0)).setColorBackground(black).setLabel("");
  tGYRO_PITCH =     controlP5.addToggle("GYRO_PITCH",true,x,y2+20,20,10).setColorActive(color(0, 255, 255)).setColorBackground(black).setLabel("");
  tGYRO_YAW =       controlP5.addToggle("GYRO_YAW",true,x,y2+30,20,10).setColorActive(color(255, 0, 255)).setColorBackground(black).setLabel("");
  tBARO   =         controlP5.addToggle("BARO",true,x,y3 ,20,10).setColorActive(color(125, 125, 125)).setColorBackground(black).setLabel("");
  tHEAD   =         controlP5.addToggle("HEAD",true,x,y4 ,20,10).setColorActive(color(225, 225, 125)).setColorBackground(black).setLabel("");
  tMAGX   =         controlP5.addToggle("MAGX",true,x,y5+10,20,10).setColorActive(color(50, 100, 150)).setColorBackground(black).setLabel("");
  tMAGY   =         controlP5.addToggle("MAGY",true,x,y5+20,20,10).setColorActive(color(100, 50, 150)).setColorBackground(black).setLabel("");
  tMAGZ   =         controlP5.addToggle("MAGZ",true,x,y5+30,20,10).setColorActive(color(150, 100, 50)).setColorBackground(black).setLabel("");
  tTEMP   =         controlP5.addToggle("TEMP",true,x,y6 ,20,10).setColorActive(color(150, 100, 50)).setColorBackground(black).setLabel("");

  controlP5.addTextlabel("acclabel","ACC",xo,y1);
  controlP5.addTextlabel("accrolllabel","   ROLL",xo,y1+10);
  controlP5.addTextlabel("accpitchlabel","   PITCH",xo,y1+20);
  controlP5.addTextlabel("acczlabel","   Z",xo,y1+30);
  controlP5.addTextlabel("gyrolabel","GYRO",xo,y2);
  controlP5.addTextlabel("gyrorolllabel","   ROLL",xo,y2+10);
  controlP5.addTextlabel("gyropitchlabel","   PITCH",xo,y2+20);
  controlP5.addTextlabel("gyroyawlabel","   YAW",xo,y2+30);
  controlP5.addTextlabel("maglabel","MAG",xo,y5);
  controlP5.addTextlabel("magrolllabel","   ROLL",xo,y5+10);
  controlP5.addTextlabel("magpitchlabel","   PITCH",xo,y5+20);
  controlP5.addTextlabel("magyawlabel","   YAW",xo,y5+30);
  controlP5.addTextlabel("altitudelabel","ALT",xo,y3);
  controlP5.addTextlabel("headlabel","HEAD",xo,y4);
  controlP5.addTextlabel("templabel","TEMP",xo,y6);

  axSlider      =    controlP5.addSlider("axSlider",-1000,+1000,0,x+20,y1+10,50,10).setDecimalPrecision(0).setLabel("");
  aySlider      =    controlP5.addSlider("aySlider",-1000,+1000,0,x+20,y1+20,50,10).setDecimalPrecision(0).setLabel("");
  azSlider      =    controlP5.addSlider("azSlider",-1000,+1000,0,x+20,y1+30,50,10).setDecimalPrecision(0).setLabel("");
  gxSlider      =    controlP5.addSlider("gxSlider",-5000,+5000,0,x+20,y2+10,50,10).setDecimalPrecision(0).setLabel("");
  gySlider      =    controlP5.addSlider("gySlider",-5000,+5000,0,x+20,y2+20,50,10).setDecimalPrecision(0).setLabel("");
  gzSlider      =    controlP5.addSlider("gzSlider",-5000,+5000,0,x+20,y2+30,50,10).setDecimalPrecision(0).setLabel("");
  altSlider     =    controlP5.addSlider("altSlider",-30000,+30000,0,x+20,y3 ,50,10).setDecimalPrecision(2).setLabel("");
  headSlider    =    controlP5.addSlider("headSlider",-200,+200,0,x+20,y4  ,50,10).setDecimalPrecision(0).setLabel("");
  magxSlider    =    controlP5.addSlider("magxSlider",-5000,+5000,0,x+20,y5+10,50,10).setDecimalPrecision(0).setLabel("");
  magySlider    =    controlP5.addSlider("magySlider",-5000,+5000,0,x+20,y5+20,50,10).setDecimalPrecision(0).setLabel("");
  magzSlider    =    controlP5.addSlider("magzSlider",-5000,+5000,0,x+20,y5+30,50,10).setDecimalPrecision(0).setLabel("");
  tempSlider    =    controlP5.addSlider("tempSlider",-400,+400,0,x+20,y6  ,50,10).setDecimalPrecision(0).setLabel("");

  buttonCALIBRATE_ACC = controlP5.addButton("CALIB_ACC",  1,  xParam+130, yParam, 80, 20).setColorBackground(red_);
  buttonCALIBRATE_MAG = controlP5.addButton("CALIB_MAG",  1,  xParam+215, yParam, 80, 20).setColorBackground(red_);

  scaleSlider = controlP5.addSlider("SCALE",0,10,1,xGraph+515,yGraph,75,20).setLabel("");// GraphScaler
}     /************* End of setup() *************/



/******************************* Multiwii Serial Protocol **********************/
private static final String MSP_HEADER = "$M<";

private static final int
  MSP_IDENT                =100,
  MSP_STATUS               =101,
  MSP_RAW_IMU              =102,
  MSP_ATTITUDE             =108,
  MSP_ALTITUDE             =109,
  MSP_ACC_CALIBRATION      =205,
  MSP_MAG_CALIBRATION      =206
;

public static final int
  IDLE = 0,
  HEADER_START = 1,
  HEADER_M = 2,
  HEADER_ARROW = 3,
  HEADER_SIZE = 4,
  HEADER_CMD = 5,
  HEADER_ERR = 6
;

int c_state = IDLE;
boolean err_rcvd = false;

byte checksum=0;
byte cmd;
int offset=0, dataSize=0;
byte[] inBuf = new byte[256];


int p;
int read32() {return (inBuf[p++]&0xff) + ((inBuf[p++]&0xff)<<8) + ((inBuf[p++]&0xff)<<16) + ((inBuf[p++]&0xff)<<24); }
int read16() {return (inBuf[p++]&0xff) + ((inBuf[p++])<<8); }
int read8()  {return  inBuf[p++]&0xff;}

int mode;
boolean toggleRead = false,toggleCalibAcc = false,toggleCalibMag = false;

//send msp without payload
private List<Byte> requestMSP(int msp) {
  return  requestMSP( msp, null);
}

//send multiple msp without payload
private List<Byte> requestMSP (int[] msps) {
  List<Byte> s = new LinkedList<Byte>();
  for (int m : msps) {
	s.addAll(requestMSP(m, null));
  }
  return s;
}

//send msp with payload
private List<Byte> requestMSP (int msp, Character[] payload) {
  if(msp < 0) {
   return null;
  }
  List<Byte> bf = new LinkedList<Byte>();
  for (byte c : MSP_HEADER.getBytes()) {
	bf.add( c );
  }

  byte checksum=0;
  byte pl_size = (byte)((payload != null ? int(payload.length) : 0)&0xFF);
  bf.add(pl_size);
  checksum ^= (pl_size&0xFF);

  bf.add((byte)(msp & 0xFF));
  checksum ^= (msp&0xFF);

  if (payload != null) {
	for (char c :payload){
	  bf.add((byte)(c&0xFF));
	  checksum ^= (c&0xFF);
	}
  }
  bf.add(checksum);
  return (bf);
}

void sendRequestMSP(List<Byte> msp) {
  byte[] arr = new byte[msp.size()];
  int i = 0;
  for (byte b: msp) {
	arr[i++] = b;
  }
  g_serial.write(arr); // send the complete byte sequence in one go
}

public void evaluateCommand(byte cmd, int dataSize) {
  int i;
  int icmd = (int)(cmd&0xFF);
  switch(icmd) {
	case MSP_IDENT:
		version = read8();
	   break;

	case MSP_STATUS:
		cycleTime = read16();
		i2cError = read16();
		present = read16();
		if ((present&1) >0) {buttonAcc.setColorBackground(green_);} else {buttonAcc.setColorBackground(red_);tACC_ROLL.setState(false); tACC_PITCH.setState(false); tACC_Z.setState(false);}
		if ((present&2) >0) {buttonBaro.setColorBackground(green_);} else {buttonBaro.setColorBackground(red_); tBARO.setState(false); }
		if ((present&4) >0) {buttonMag.setColorBackground(green_); Mag_=true;} else {buttonMag.setColorBackground(red_); tMAGX.setState(false); tMAGY.setState(false); tMAGZ.setState(false);}
		break;
	case MSP_RAW_IMU:
		ax = read16();ay = read16();az = read16();
		gx = read16()/8;gy = read16()/8;gz = read16()/8;
		magx = read16()/3;magy = read16()/3;magz = read16()/3;
                break;
	case MSP_ATTITUDE:
		angx = read16()/10;angy = read16()/10;
		head = read16(); break;
	case MSP_ALTITUDE:
	    alt = read32();
	    read16();
	    temp = read32()/100; break;
	case MSP_ACC_CALIBRATION:break;
	case MSP_MAG_CALIBRATION:break;
	default:
		//println("Don't know how to handle reply "+icmd);
  }
}

private int present = 0;
int time,time2,time3,time4,time5,time6;

void draw() {
  List<Character> payload;
  int i,aa;
  float val,inter,a,b,h;
  int c;
  if (init_com==1 && graph_on==1) {
	time=millis();

	if ((time-time4)>40) {
	  time4=time;
	  accROLL.addVal(ax);accPITCH.addVal(ay);accYAW.addVal(az);
      gyroROLL.addVal(gx);gyroPITCH.addVal(gy);gyroYAW.addVal(gz);
	  magxData.addVal(magx);magyData.addVal(magy);magzData.addVal(magz);
	  altData.addVal(alt);headData.addVal(head);tempData.addVal(temp);
	}

	if (!toggleRead) {
	  if ((time-time2)>40  ){
		time2=time;
		int[] requests = {MSP_STATUS, MSP_RAW_IMU};
		sendRequestMSP(requestMSP(requests));
	  }
	  if ((time-time3)>25 ) {
		time3=time;
		int[] requests = {MSP_ATTITUDE};
		sendRequestMSP(requestMSP(requests));
	  }
	  if ((time-time5)>100 ) {
		time5=time;
		int[] requests = {MSP_ALTITUDE};
		sendRequestMSP(requestMSP(requests));
	  }
	}
	if (toggleRead) {
	  int[] requests = {MSP_IDENT};
	  sendRequestMSP(requestMSP(requests));
	  toggleRead=false;
	}
	if (toggleCalibAcc) {
	  toggleCalibAcc=false;
	  sendRequestMSP(requestMSP(MSP_ACC_CALIBRATION));
	}
	if (toggleCalibMag) {
	  toggleCalibMag=false;
	  sendRequestMSP(requestMSP(MSP_MAG_CALIBRATION));
	}

	while (g_serial.available()>0) {
	  c = (g_serial.read());

	  if (c_state == IDLE) {
		c_state = (c=='$') ? HEADER_START : IDLE;
	  } else if (c_state == HEADER_START) {
		c_state = (c=='M') ? HEADER_M : IDLE;
	  } else if (c_state == HEADER_M) {
		if (c == '>') {
		  c_state = HEADER_ARROW;
		} else if (c == '!') {
		  c_state = HEADER_ERR;
		} else {
		  c_state = IDLE;
		}
	  } else if (c_state == HEADER_ARROW || c_state == HEADER_ERR) {
		/* is this an error message? */
		err_rcvd = (c_state == HEADER_ERR);        /* now we are expecting the payload size */
		dataSize = (c&0xFF);
		/* reset index variables */
		p = 0;
		offset = 0;
		checksum = 0;
		checksum ^= (c&0xFF);
		/* the command is to follow */
		c_state = HEADER_SIZE;
	  } else if (c_state == HEADER_SIZE) {
		cmd = (byte)(c&0xFF);
		checksum ^= (c&0xFF);
		c_state = HEADER_CMD;
	  } else if (c_state == HEADER_CMD && offset < dataSize) {
		  checksum ^= (c&0xFF);
		  inBuf[offset++] = (byte)(c&0xFF);
	  } else if (c_state == HEADER_CMD && offset >= dataSize) {
		/* compare calculated and transferred checksum */
		if ((checksum&0xFF) == (c&0xFF)) {
		  if (err_rcvd) {
			//System.err.println("Copter did not understand request type "+c);
		  } else {
			/* we got a valid response packet, evaluate it */
			evaluateCommand(cmd, (int)dataSize);
		  }
		} else {
		  System.out.println("invalid checksum for command "+((int)(cmd&0xFF))+": "+(checksum&0xFF)+" expected, got "+(int)(c&0xFF));
		  System.out.print("<"+(cmd&0xFF)+" "+(dataSize&0xFF)+"> {");
		  for (i=0; i<dataSize; i++) {
			if (i!=0) { System.err.print(' '); }
			System.out.print((inBuf[i] & 0xFF));
		  }
		  System.out.println("} ["+c+"]");
		  System.out.println(new String(inBuf, 0, dataSize));
		}
		c_state = IDLE;
	  }
	}
  }

  // Main background colour
  background(80);
  // Version Background
  fill(40, 40, 40);
  strokeWeight(0);
//  stroke(0);
//  rectMode(CORNERS);
  rect(0,0,windowsX,tabHeight);
  textFont(font15);
  // version
  fill(255, 255, 255);
  text("",16,19+tabHeight);
  text("V",16,35+tabHeight);
  text(version, 27, 35+tabHeight);
//  text(i2cError,xGraph+410,yGraph-10);
//  text(cycleTime,xGraph+290,yGraph-10);

//  text("I2C error:",xGraph+350,yGraph-10);
//  text("Cycle Time:",xGraph+220,yGraph-10);

  fill(255,255,255);

  axSlider.setValue(ax);aySlider.setValue(ay);azSlider.setValue(az);
  gxSlider.setValue(gx);gySlider.setValue(gy);gzSlider.setValue(gz);
  magxSlider.setValue(magx);magySlider.setValue(magy);magzSlider.setValue(magz);
  altSlider.setValue(alt/100);headSlider.setValue(head);tempSlider.setValue(temp);

  stroke(255);
  a=radians(angx);
  if (angy<-90) {b=radians(-180 - angy);}
  else if (angy>90) {b=radians(+180 - angy);}
  else{ b=radians(angy);}
  h=radians(head);

  // ---------------------------------------------------------------------------------------------
  // Draw 3D object
  // ---------------------------------------------------------------------------------------------
  float size = 80.0;
  // object
  fill(255,255,255);
  pushMatrix();
  camera();
  translate(xObj,yObj);
  directionalLight(200,200,200, 0, 0, -1);
  rotateZ(h);rotateX(b);rotateY(a);
  stroke(150,255,150);
  strokeWeight(0);box(size,size*1.5,size/4);strokeWeight(3);
  line(0,0, 10,0,-size-5,10);line(0,-size-5,10,+size/4,-size/2,10); line(0,-size-5,10,-size/4,-size/2,10);
  noLights();camera();popMatrix();

  // ---------------------------------------------------------------------------------------------
  // Fly Level Control Instruments
  // ---------------------------------------------------------------------------------------------
  // info angles
  fill(255,255,127);
  textFont(font12);
  text((int)angy + "°", xLevelObj+50, yLevelObj+70); //pitch
  text((int)angx + "°", xLevelObj-68, yLevelObj+70); //roll

  pushMatrix();
  translate(xLevelObj-104,yLevelObj+42);
  fill(50,50,50);
  noStroke();
  ellipse(0,0,66,66);
  rotate(a);
  fill(255,255,127);
  textFont(font12);text("ROLL", -13, 15);
  strokeWeight(1.5);
  stroke(127,127,127);
  line(-30,1,30,1);
  stroke(255,255,255);
  line(-30,0,+30,0);line(0,0,0,-10);
  popMatrix();

  pushMatrix();
  translate(xLevelObj+104,yLevelObj+42);
  fill(50,50,50);
  noStroke();
  ellipse(0,0,66,66);
  rotate(b);
  fill(255,255,127);
  textFont(font12);text("PITCH", -18, 15);
  strokeWeight(1.5);
  stroke(127,127,127);
  line(-30,1,30,1);
  stroke(255,255,255);
  line(-30,0,30,0);line(30,0,25,5);line(+30,0,25,-5);
  popMatrix();

  // ---------------------------------------------------------------------------------------------
  // Magnetron Combi Fly Level Control
  // ---------------------------------------------------------------------------------------------
  horizonInstrSize=68;
  angyLevelControl=((angy<-horizonInstrSize) ? -horizonInstrSize : (angy>horizonInstrSize) ? horizonInstrSize : angy);
  pushMatrix();
  translate(xLevelObj,yLevelObj);
  noStroke();
  // instrument background
  fill(50,50,50);
  ellipse(0,0,150,150);
  // full instrument
  rotate(-a);
  rectMode(CORNER);
  // outer border
  strokeWeight(1);
  stroke(90,90,90);
  //border ext
  arc(0,0,140,140,0,TWO_PI);
  stroke(190,190,190);
  //border int
  arc(0,0,138,138,0,TWO_PI);
  // inner quadrant
  strokeWeight(1);
  stroke(255,255,255);
  fill(124,73,31);
  //earth
  float angle = acos(angyLevelControl/horizonInstrSize);
  arc(0,0,136,136,0,TWO_PI);
  fill(38,139,224);
  //sky
  arc(0,0,136,136,HALF_PI-angle+PI,HALF_PI+angle+PI);
  float x = sin(angle)*horizonInstrSize;
  if (angy>0)
	fill(124,73,31);
  noStroke();
  triangle(0,0,x,-angyLevelControl,-x,-angyLevelControl);
  // inner lines
  strokeWeight(1);
  for(i=0;i<8;i++) {
	j=i*15;
	if (angy<=(35-j) && angy>=(-65-j)) {
	  stroke(255,255,255); line(-30,-15-j-angy,30,-15-j-angy); // up line
	  fill(255,255,255);
	  textFont(font9);
	  text("+" + (i+1) + "0", 34, -12-j-angy); //  up value
	  text("+" + (i+1) + "0", -48, -12-j-angy); //  up value
	}
	if (angy<=(42-j) && angy>=(-58-j)) {
	  stroke(167,167,167); line(-20,-7-j-angy,20,-7-j-angy); // up semi-line
	}
	if (angy<=(65+j) && angy>=(-35+j)) {
	  stroke(255,255,255); line(-30,15+j-angy,30,15+j-angy); // down line
	  fill(255,255,255);
	  textFont(font9);
	  text("-" + (i+1) + "0", 34, 17+j-angy); //  down value
	  text("-" + (i+1) + "0", -48, 17+j-angy); //  down value
	}
	if (angy<=(58+j) && angy>=(-42+j)) {
	  stroke(127,127,127); line(-20,7+j-angy,20,7+j-angy); // down semi-line
	}
  }
  strokeWeight(2);
  stroke(255,255,255);
  if (angy<=50 && angy>=-50) {
	line(-40,-angy,40,-angy); //center line
	fill(255,255,255);
	textFont(font9);
	text("0", 34, 4-angy); // center
	text("0", -39, 4-angy); // center
  }

  // lateral arrows
  strokeWeight(1);
  // down fixed triangle
  stroke(60,60,60);
  fill(180,180,180,255);

  triangle(-horizonInstrSize,-8,-horizonInstrSize,8,-55,0);
  triangle(horizonInstrSize,-8,horizonInstrSize,8,55,0);

  // center
  strokeWeight(1);
  stroke(255,0,0);
  line(-20,0,-5,0); line(-5,0,-5,5);
  line(5,0,20,0); line(5,0,5,5);
  line(0,-5,0,5);
  popMatrix();


  // ---------------------------------------------------------------------------------------------
  // Compass Section
  // ---------------------------------------------------------------------------------------------
  pushMatrix();
  translate(xCompass,yCompass);
  // Compass background
  noStroke();
  fill(50, 50, 50);
  ellipse(0,0,150,150);
  strokeWeight(3);stroke(0);
  rectMode(CORNERS);
  size=34;
//  rect(-size*2.5,-size*2.5,size*2.5,size*2.5);
  // compass quadrant background
  strokeWeight(2);
  fill(120);stroke(190);
  ellipse(0,  0,   4*size+4, 4*size+4);
  // compass quadrant
  strokeWeight(1.5);fill(0);stroke(0);
  ellipse(0,  0,   2.6*size+7, 2.6*size+7);
  // Compass rotating pointer
  stroke(255);
  rotate(head*PI/180);
  line(0,size*0.2, 0,-size*1.3); line(0,-size*1.3, -5 ,-size*1.3+10); line(0,-size*1.3, +5 ,-size*1.3+10);
  popMatrix();
  // angles
  for (i=0;i<=12;i++) {
	angCalc=i*PI/6;
	if (i%3!=0) {
	  stroke(75);
	  line(xCompass+cos(angCalc)*size*2,yCompass+sin(angCalc)*size*2,xCompass+cos(angCalc)*size*1.6,yCompass+sin(angCalc)*size*1.6);
	} else {
	  stroke(255);
	  line(xCompass+cos(angCalc)*size*2.2,yCompass+sin(angCalc)*size*2.2,xCompass+cos(angCalc)*size*1.9,yCompass+sin(angCalc)*size*1.9);
	}
  }
  textFont(font15);
  text("N", xCompass-5, yCompass-22-size*0.9);text("S", xCompass-5, yCompass+32+size*0.9);
  text("W", xCompass-33-size*0.9, yCompass+6);text("E", xCompass+21+size*0.9, yCompass+6);
  // head indicator
  textFont(font12);
  noStroke();
  fill(80,80,80,130);
  rect(xCompass-22,yCompass-8,xCompass+22,yCompass+9);
  fill(255,255,127);
  text(head + "°",xCompass-11-(head>=10.0 ? (head>=100.0 ? 6 : 3) : 0),yCompass+6);

  // ---------------------------------------------------------------------------------------------
  // Temperature Section
  // ---------------------------------------------------------------------------------------------
  pushMatrix();
  translate(xTempObj,yTempObj);
  // background
  noStroke();
  fill(50, 50, 50);
  ellipse(0,0,150,150);
  strokeWeight(3);stroke(0);
  rectMode(CORNERS);
  size=34;
  // compass quadrant background
  strokeWeight(2);
  fill(120);stroke(190);
  ellipse(0,  0,   4*size+4, 4*size+4);
  // compass quadrant
  strokeWeight(1.5);fill(0);stroke(0);
  ellipse(0,  0,   3*size+10, 3*size+10);
  popMatrix();
  textFont(font40);
  noStroke();
  fill(255,255,127);
  text(round(temp),xTempObj-25,yTempObj+10);
  textFont(font15);
  fill(255,255,127);
  text("°C",xTempObj-10,yTempObj+40);

  // ---------------------------------------------------------------------------------------------
  // GRAPH
  // ---------------------------------------------------------------------------------------------
  strokeWeight(1);
  fill(255, 255, 255);
  g_graph.drawGraphBox();

  strokeWeight(1.5);
  stroke(255, 0, 0); if (axGraph) g_graph.drawLine(accROLL, -1000, +1000);
  stroke(0, 255, 0); if (ayGraph) g_graph.drawLine(accPITCH, -1000, +1000);
  stroke(0, 0, 255);
  if (azGraph) {
	if (scaleSlider.value()<2){ g_graph.drawLine(accYAW, -1000, +1000);
	} else{ g_graph.drawLine(accYAW, 200*scaleSlider.value()-1000,200*scaleSlider.value()+500);}
  }

  float altMin = (altData.getMinVal() + altData.getRange() / 2) - 100;
  float altMax = (altData.getMaxVal() + altData.getRange() / 2) + 100;

  stroke(200, 200, 0);  if (gxGraph)   g_graph.drawLine(gyroROLL, -300, +300);
  stroke(0, 255, 255);  if (gyGraph)   g_graph.drawLine(gyroPITCH, -300, +300);
  stroke(255, 0, 255);  if (gzGraph)   g_graph.drawLine(gyroYAW, -300, +300);
  stroke(125, 125, 125);if (altGraph)  g_graph.drawLine(altData, altMin, altMax);
  stroke(225, 225, 125);if (headGraph) g_graph.drawLine(headData, -370, +370);
  stroke(50, 100, 150); if (magxGraph) g_graph.drawLine(magxData, -500, +500);
  stroke(100, 50, 150); if (magyGraph) g_graph.drawLine(magyData, -500, +500);
  stroke(150, 100, 50); if (magzGraph) g_graph.drawLine(magzData, -500, +500);
  stroke(225, 225, 125);if (tempGraph) g_graph.drawLine(tempData, -40, +40);

}

void ACC_ROLL(boolean theFlag) {axGraph = theFlag;}
void ACC_PITCH(boolean theFlag) {ayGraph = theFlag;}
void ACC_Z(boolean theFlag) {azGraph = theFlag;}
void GYRO_ROLL(boolean theFlag) {gxGraph = theFlag;}
void GYRO_PITCH(boolean theFlag) {gyGraph = theFlag;}
void GYRO_YAW(boolean theFlag) {gzGraph = theFlag;}
void BARO(boolean theFlag) {altGraph = theFlag;}
void HEAD(boolean theFlag) {headGraph = theFlag;}
void MAGX(boolean theFlag) {magxGraph = theFlag;}
void MAGY(boolean theFlag) {magyGraph = theFlag;}
void MAGZ(boolean theFlag) {magzGraph = theFlag;}
void TEMP(boolean theFlag) {tempGraph = theFlag;}

public void controlEvent(ControlEvent theEvent) {
  if (theEvent.isGroup()) if (theEvent.name()=="portComList") InitSerial(theEvent.group().value()); // initialize the serial port selected
}

public void bSTART() {
  if(graphEnabled == false) {return;}
  graph_on=1;
  toggleRead=true;
  g_serial.clear();
}

public void bSTOP() {
  graph_on=0;
}

public void CALIB_ACC() {toggleCalibAcc = true;}
public void CALIB_MAG() {toggleCalibMag = true;}

// initialize the serial port selected in the listBox
void InitSerial(float portValue) {
  println("Serial init");
  if (portValue < commListMax) {
	String portPos = Serial.list()[int(portValue)];
	//txtlblWhichcom.setValue("COM = " + shortifyPortName(portPos, 8));
	g_serial = new Serial(this, portPos, GUI_BaudRate);
	SaveSerialPort(portPos);
	init_com=1;
	buttonSTART.setColorBackground(green_);buttonSTOP.setColorBackground(green_);
	commListbox.setColorBackground(green_);
	buttonCALIBRATE_ACC.setColorBackground(green_); buttonCALIBRATE_MAG.setColorBackground(green_);
	graphEnabled = true;
	g_serial.buffer(256);
	btnQConnect.hide();
  } else {
	//txtlblWhichcom.setValue("Comm Closed");
	init_com=0;
	buttonSTART.setColorBackground(red_);buttonSTOP.setColorBackground(red_);commListbox.setColorBackground(red_);
	graphEnabled = false;
	init_com=0;
	g_serial.stop();
	btnQConnect.show();
  }
}

void SaveSerialPort(String port ) {
	output = createWriter(portnameFile);
	output.print( port + ';' + GUI_BaudRate); // Write the comport to the file
	output.flush(); // Writes the remaining data to the file
	output.close(); // Finishes the file
 }

public void bQCONN(){
  ReadSerialPort();
  InitSerial(SerialPort);
  bSTART();
  toggleRead=true;
}

void ReadSerialPort() {
	reader = createReader(portnameFile);
	String line;
  try {
	line = reader.readLine();
  } catch (IOException e) {
	e.printStackTrace();
	line = null; }
  if (line == null) {
	// Stop reading because of an error or file is empty
	// Roll on with no input
	btnQConnect.hide();
   return;
  } else {
	String[] pieces = split(line, ';');
	int Port = int(pieces[0]);
	GUI_BaudRate= int(pieces[1]);
	if (commListMax==1){}
	String pPort=pieces[0];
	for( i=0;i<commListMax;i++) {
	  String[] pn =  match(shortifyPortName(Serial.list()[i], 13),  pieces[0]);
	  if ( pn !=null){ SerialPort=i;}
	}
  }
}
//********************************************************

class cDataArray {
  float[] m_data;
  int m_maxSize, m_startIndex = 0, m_endIndex = 0, m_curSize;

  cDataArray(int maxSize){
	m_maxSize = maxSize;
	m_data = new float[maxSize];
  }
  void addVal(float val) {
	m_data[m_endIndex] = val;
	m_endIndex = (m_endIndex+1)%m_maxSize;
	if (m_curSize == m_maxSize) {
	  m_startIndex = (m_startIndex+1)%m_maxSize;
	} else {
	  m_curSize++;
	}
  }
  float getVal(int index) {return m_data[(m_startIndex+index)%m_maxSize];}
  int getCurSize(){return m_curSize;}
  int getMaxSize() {return m_maxSize;}
  float getMaxVal() {
	float res = 0.0;
	for( i=0; i<m_curSize-1; i++) if ((m_data[i] > res) || (i==0)) res = m_data[i];
	return res;
  }
  float getMinVal() {
	float res = 0.0;
	for( i=0; i<m_curSize-1; i++) if ((m_data[i] < res) || (i==0)) res = m_data[i];
	return res;
  }
  float getRange() {return getMaxVal() - getMinVal();}
}

// This class takes the data and helps graph it
class cGraph {
  float m_gWidth, m_gHeight, m_gLeft, m_gBottom, m_gRight, m_gTop;

  cGraph(float x, float y, float w, float h) {
	m_gWidth     = w; m_gHeight    = h;
	m_gLeft      = x; m_gBottom    = y;
	m_gRight     = x + w;
	m_gTop       = y + h;
  }

  void drawGraphBox() {
	stroke(0, 0, 0);
	rectMode(CORNERS);
	rect(m_gLeft, m_gBottom, m_gRight, m_gTop);
  }

  void drawLine(cDataArray data, float minRange, float maxRange) {
	float graphMultX = m_gWidth/data.getMaxSize();
	float graphMultY = m_gHeight/(maxRange-minRange);

	for( i=0; i<data.getCurSize()-1; ++i) {
	  float x0 = i*graphMultX+m_gLeft;
	  float y0 = m_gTop-(((data.getVal(i)-(maxRange+minRange)/2)*scaleSlider.value()+(maxRange-minRange)/2)*graphMultY);
	  float x1 = (i+1)*graphMultX+m_gLeft;
	  float y1 = m_gTop-(((data.getVal(i+1)-(maxRange+minRange)/2 )*scaleSlider.value()+(maxRange-minRange)/2)*graphMultY);
	  line(x0, y0, x1, y1);
	}
  }
}

