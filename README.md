# STEM-SensorLearningBoard #

This is the code repository for an Arduino hooked up to sensors used in a STEM curriculum for K12 kids.

#### Credits ####
Code from the MultiWii and MultiWiiConf projects found at http://www.multiwii.com was used in this project.