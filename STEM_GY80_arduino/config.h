#ifndef CONFIG_H_
#define CONFIG_H_

/* Proc auto detection */
#if defined(__AVR_ATmega168__) || defined(__AVR_ATmega328P__)
  #define PROMINI
#endif

/* atmega328P (Promini) */
#if defined(PROMINI)
  #define LEDPIN_PINMODE             pinMode (13, OUTPUT);
  #define LEDPIN_TOGGLE              PINB |= 1<<5;     //switch LEDPIN state (digital PIN 13)
  #define LEDPIN_OFF                 PORTB &= ~(1<<5);
  #define LEDPIN_ON                  PORTB |= (1<<5);
  #define LEDPIN_GYRO_PINMODE        pinMode (A1, OUTPUT);
  #define LEDPIN_GYRO_OFF            PORTC &= ~(1<<1);
  #define LEDPIN_GYRO_ON             PORTC |= (1<<1);
  #define LEDPIN_MAG_PINMODE         pinMode (A2, OUTPUT);
  #define LEDPIN_MAG_OFF             PORTC &= ~(1<<2);
  #define LEDPIN_MAG_ON              PORTC |= (1<<2);
  #define LEDPIN_BARO_PINMODE        pinMode (A3, OUTPUT);
  #define LEDPIN_BARO_OFF            PORTC &= ~(1<<3);
  #define LEDPIN_BARO_ON             PORTC |= (1<<3);
  #define BUZZERPIN_PINMODE          pinMode (8, OUTPUT);
  #define BUZZERPIN_ON               PORTB |= 1;
  #define BUZZERPIN_OFF           	 PORTB &= ~1;
  #define I2C_PULLUPS_ENABLE         PORTC |= 1<<4; PORTC |= 1<<5;   // PIN A4&A5 (SDA&SCL)
  #define I2C_PULLUPS_DISABLE        PORTC &= ~(1<<4); PORTC &= ~(1<<5);
#endif

#define SERIAL0_COM_SPEED 57600
#define SERIAL1_COM_SPEED 57600
#define SERIAL2_COM_SPEED 57600
#define SERIAL3_COM_SPEED 57600

#define I2C_SPEED 100000L
#define NEUTRALIZE_DELAY 100000 // when there is an error on I2C bus, we neutralize the values during a short time.

/* Sensor Boards */
//Gyroscope & Accelerometer GY-521
#define MPU6050
#define GYRO 1
//#define GYRO_ORIENTATION(X, Y, Z) {imu.gyroADC[ROLL] =  Y; imu.gyroADC[PITCH] = -X; imu.gyroADC[YAW] = -Z;}
#define GYRO_ORIENTATION(X, Y, Z) {imu.gyroADC[ROLL] =  X; imu.gyroADC[PITCH] = Y; imu.gyroADC[YAW] = -Z;}
#define GYRO_SCALE (4 / 16.4 * PI / 180.0 / 1000000.0)   //MPU6050 and MPU3050   16.4 LSB/(deg/s) and we ignore the last 2 bits
#define ACC 1
//#define ACC_ORIENTATION(X, Y, Z)  {imu.accADC[ROLL]  = -X; imu.accADC[PITCH]  = -Y; imu.accADC[YAW]  =  Z;}
#define ACC_ORIENTATION(X, Y, Z)  {imu.accADC[ROLL]  = Y; imu.accADC[PITCH]  = -X; imu.accADC[YAW]  =  Z;}
#define ACC_1G 512
#define ACCZ_25deg   (int16_t)(ACC_1G * 0.90631) // 0.90631 = cos(25deg) (cos(theta) of accZ comparison)
#define ACC_VelScale (9.80665f / 10000.0f / ACC_1G)
#undef INTERNAL_I2C_PULLUPS

//Magnetometer GY-273
#define HMC5883
#define MAG 1
//#define MAG_ORIENTATION(X, Y, Z)  {imu.magADC[ROLL]  =  X; imu.magADC[PITCH]  =  Y; imu.magADC[YAW]  = -Z;}
#define MAG_ORIENTATION(X, Y, Z)  {imu.magADC[ROLL]  =  -Y; imu.magADC[PITCH]  =  X; imu.magADC[YAW]  = -Z;}

//Barometer GY-68
#define BMP085
#define BARO 1

//GPS
#define GPS 0

//Sonar
#define SONAR 0

//#define BUZZER

#endif /* CONFIG_H_ */
