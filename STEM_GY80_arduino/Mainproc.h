#ifndef MAINPROC_H_
#define MAINPROC_H_

#define  VERSION  10

#include "types.h"

extern uint32_t currentTime;
extern uint16_t previousTime;
extern uint16_t cycleTime;
extern uint16_t calibratingA;
extern uint16_t calibratingB;
extern uint16_t calibratingG;
extern int16_t  sonarAlt;

extern int16_t  i2c_errors_count;
extern uint8_t alarmArray[16];
extern global_conf_t global_conf;

extern imu_t imu;
extern alt_t alt;
extern att_t att;

extern conf_t conf;

extern int16_t  annex650_overrun_count;
extern flags_struct_t f;

extern int16_t gyroZero[3];
extern int16_t angle[2];

#if BARO
  extern int32_t baroPressure;
  extern int32_t baroTemperature;
  extern int32_t baroPressureSum;
#endif

void annexCode();

#endif /* MAINPROC_H_ */
