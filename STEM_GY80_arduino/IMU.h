#ifndef IMU_H_
#define IMU_H_

#define BARO_TAB_SIZE   21

#if BARO
uint8_t getEstimatedAltitude();
uint8_t getTemperature();
#endif

void computeIMU();

#endif /* IMU_H_ */
